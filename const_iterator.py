from typing import List, Tuple, Dict, Union, Generator, Any

_DICT_OF_CONSTANTS = Dict[str, Tuple[Union[int, float], Union[int, float, None], Union[int, float, None]]]


def _gen_value(start, stop=0, step=0):
    """Generate a sequence of constant values.

    Works like the original range, only with floats.
    The problem is due to float precision, sometimes the last value
    is not considered due to the fact that there is a tail .000000005, etc.

    :argument start the initial value of the constant
    :argument stop maximum constant value
    :argument step changing the value in one iteration
    """
    value = float(start)
    stop = float(stop)
    step = float(step)

    yield start

    while True:
        value += step
        if value > stop or step == 0:
            break
        yield value


def _gen_constants_values(constants_info):
    """Generate a set of constants.

    In one iteration, simultaneously changes the values of all constants.

    :argument constants_info dict, where
        key - constant name,
        value - tuple of information for generating constants.

    :return Constants dict, где key-constant name, value-constant value.
    """
    constants_generators = {name: _gen_value(*info) for name, info in constants_info.items()}
    result = {}
    while True:
        try:
            for name, generator in constants_generators.items():
                result[name] = next(generator)
        except StopIteration:
            break
        yield result


def _get_constant_group(constants_groups, const):
    """Return the names of all constants from a constant group."""
    if constants_groups:
        for group in constants_groups:
            if const in group:
                const_name_index = group.index(const)
                return group[:const_name_index] + group[const_name_index + 1:]


def _get_constants_generator(dict_of_constants_info, constants_groups=None, **kwargs)\
        -> Generator[Dict[str, float], Any, None]:
    """Generate a dictionary of constants.

    Works recursively, descending recursively at the beginning
    to the last constant, passes through its generator completely,
    then takes a step back, changing the second to last constant, and so on.
    Thus, it returns all possible sets of constants.

    If constants_groups is passed, then the groups of constants
    change simultaneously for 1 iteration.
        :return Returns a dictionary of constants in one iteration
            key - constant name,
            value - constant value.
    """
    cur_const_info = dict_of_constants_info.popitem()
    constants_to_get_on_this_step = {cur_const_info[0]: cur_const_info[1]}

    constants_from_the_same_group = _get_constant_group(constants_groups, cur_const_info[0])
    if constants_from_the_same_group:
        for const_name in constants_from_the_same_group:
            const_info = dict_of_constants_info.pop(const_name)
            constants_to_get_on_this_step[const_name] = const_info

    for constants_values in _gen_constants_values(constants_to_get_on_this_step):
        kwargs.update(constants_values)

        if dict_of_constants_info:
            for full_set_of_required_constants in _get_constants_generator(
                    dict_of_constants_info.copy(), constants_groups, **kwargs):
                yield full_set_of_required_constants
        else:
            yield kwargs


class ConstantsIterator:
    """An iterator that generates constant dictionaries.

    Works recursively, descending recursively at the beginning
    to the last constant, passes through its generator completely,
    then takes a step back, changing the second to last constant, and so on.
    Thus, it returns all possible sets of constants.

    Example:

        dict_of_constants = {
            'alpha': (0.1, 0.6, 0.1),
            'beta': (1, 1.61, 0.1),
            'gamma': (3, 3.51, 0.1),
            'delta': (1, 2, 0.2),
        }
        groups = [('alpha', 'gamma'), ('beta', 'delta')]
        for constants in ConstantsIterator(dict_of_constants, groups):
            .....do smth......


    Generated constants:

        | alpha | gamma | beta | delta |
        | 0.10  | 3.00  | 1.00 | 1.00  |
        | 0.20  | 3.10  | 1.00 | 1.00  |
        | 0.30  | 3.20  | 1.00 | 1.00  |
        | 0.40  | 3.30  | 1.00 | 1.00  |
        | 0.50  | 3.40  | 1.00 | 1.00  |
        | 0.60  | 3.50  | 1.00 | 1.00  |
        | 0.10  | 3.00  | 1.10 | 1.20  |
        | 0.20  | 3.10  | 1.10 | 1.20  |
        | 0.30  | 3.20  | 1.10 | 1.20  |
        | 0.40  | 3.30  | 1.10 | 1.20  |
        | 0.50  | 3.40  | 1.10 | 1.20  |
        | 0.60  | 3.50  | 1.10 | 1.20  |
        | 0.10  | 3.00  | 1.20 | 1.40  |
        | 0.20  | 3.10  | 1.20 | 1.40  |
        | 0.30  | 3.20  | 1.20 | 1.40  |
        | 0.40  | 3.30  | 1.20 | 1.40  |
        | 0.50  | 3.40  | 1.20 | 1.40  |
        | 0.60  | 3.50  | 1.20 | 1.40  |
        | 0.10  | 3.00  | 1.30 | 1.60  |
        | 0.20  | 3.10  | 1.30 | 1.60  |
        | 0.30  | 3.20  | 1.30 | 1.60  |
        | 0.40  | 3.30  | 1.30 | 1.60  |
        | 0.50  | 3.40  | 1.30 | 1.60  |
        | 0.60  | 3.50  | 1.30 | 1.60  |
        | 0.10  | 3.00  | 1.40 | 1.80  |
        | 0.20  | 3.10  | 1.40 | 1.80  |
        | 0.30  | 3.20  | 1.40 | 1.80  |
        | 0.40  | 3.30  | 1.40 | 1.80  |
        | 0.50  | 3.40  | 1.40 | 1.80  |
        | 0.60  | 3.50  | 1.40 | 1.80  |
        | 0.10  | 3.00  | 1.50 | 2.00  |
        | 0.20  | 3.10  | 1.50 | 2.00  |
        | 0.30  | 3.20  | 1.50 | 2.00  |
        | 0.40  | 3.30  | 1.50 | 2.00  |
        | 0.50  | 3.40  | 1.50 | 2.00  |
        | 0.60  | 3.50  | 1.50 | 2.00  |

    """

    def __init__(
            self,
            dict_of_constants_info: _DICT_OF_CONSTANTS,
            groups_of_constants: List[Tuple[str, ...]] = None):
        """
        :param dict_of_constants_info - constant dict.
            The dictionary key must be the name of the constant
            The value must be a tuple consisting of 3 int or float numbers
            1 number - The initial value of the constant;
            2 number - The final value of the constant;
            3 number - Constant change step.
            Example:
                dict_of_constants = {
                    'alpha': (0.1, 0.5, 0.1),
                    'beta': (1, 1.6, 0.1),
                    'gamma': (3, 3.5, 0.1),
                }
            If it is necessary that the constant does not change, then
            just enter a tuple of 1 element (don't forget the comma after the value)
            Example:
                dict_of_constants = {
                    'alpha': (0.1,),
                    'beta': (1, 1.6, 0.1),
                    'gamma': (3, 3.5, 0.1),
                }
        :param groups_of_constants - list of constant groups.
            Optional argument.
            Groups must be passed in a tuple and
            consist of the names of the constants passed in the dictionary.
            Must be used if you want some constants
            changed simultaneously in one iteration
            Example: groups_of_constants = [('alpha','gamma')]
        """
        self.groups_of_constants = groups_of_constants
        self.dict_of_constants = dict_of_constants_info

    def __iter__(self):
        """
        Returns the generator when called with
        for constants in ConstantsIterator(dict_of_constants_info):
            ...
        constants - dictionary of constants.
        Or using iter(ConstantsIterator(dict_of_constants_info)).
        :return constant dictionary generator
        """
        return _get_constants_generator(self.dict_of_constants, self.groups_of_constants)


if __name__ == '__main__':
    dict_of_constants = {
        'alpha': (0.1, 0.6, 0.1),
        'beta': (1, 1.61, 0.1),
        'gamma': (3, 3.51, 0.1),
        'delta': (1, 2, 0.2),
    }
    groups = [('alpha', 'gamma'), ('beta', 'delta')]

    hat = '| alpha | gamma | beta | delta |'
    print(hat)
    for constants in ConstantsIterator(dict_of_constants, groups):
        row = '|{alpha:^7.2f}|{gamma:^7.2f}|{beta:^6.2f}|{delta:^7.2f}|'.format(**constants)
        print(row)
