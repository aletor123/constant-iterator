This script will be useful if you need to test some kind of algorithm, the result of which depends on constants. Entering data for iteration is very simple and straightforward.
You can set any number of constants, their initial value, final value, and the step with which its value should change like in range() from the original python library.

Example:

        dict_of_constants = {
            'alpha': (0.1, 0.6, 0.1),
            'beta': (1, 1.61, 0.1),
            'gamma': (3, 3.51, 0.1),
            'delta': (1, 2, 0.2),
        }
        groups = [('alpha', 'gamma'), ('beta', 'delta')]
        for constants in ConstantsIterator(dict_of_constants, groups):
            .....do smth......

Generated constants:

    | alpha | gamma | beta | delta |
    | 0.10  | 3.00  | 1.00 | 1.00  |
    | 0.20  | 3.10  | 1.00 | 1.00  |
    | 0.30  | 3.20  | 1.00 | 1.00  |
    | 0.40  | 3.30  | 1.00 | 1.00  |
    | 0.50  | 3.40  | 1.00 | 1.00  |
    | 0.60  | 3.50  | 1.00 | 1.00  |
    | 0.10  | 3.00  | 1.10 | 1.20  |
    | 0.20  | 3.10  | 1.10 | 1.20  |
    | 0.30  | 3.20  | 1.10 | 1.20  |
    | 0.40  | 3.30  | 1.10 | 1.20  |
    | 0.50  | 3.40  | 1.10 | 1.20  |
    | 0.60  | 3.50  | 1.10 | 1.20  |
    | 0.10  | 3.00  | 1.20 | 1.40  |
    | 0.20  | 3.10  | 1.20 | 1.40  |
    | 0.30  | 3.20  | 1.20 | 1.40  |
    | 0.40  | 3.30  | 1.20 | 1.40  |
    | 0.50  | 3.40  | 1.20 | 1.40  |
    | 0.60  | 3.50  | 1.20 | 1.40  |
    | 0.10  | 3.00  | 1.30 | 1.60  |
    | 0.20  | 3.10  | 1.30 | 1.60  |
    | 0.30  | 3.20  | 1.30 | 1.60  |
    | 0.40  | 3.30  | 1.30 | 1.60  |
    | 0.50  | 3.40  | 1.30 | 1.60  |
    | 0.60  | 3.50  | 1.30 | 1.60  |
    | 0.10  | 3.00  | 1.40 | 1.80  |
    | 0.20  | 3.10  | 1.40 | 1.80  |
    | 0.30  | 3.20  | 1.40 | 1.80  |
    | 0.40  | 3.30  | 1.40 | 1.80  |
    | 0.50  | 3.40  | 1.40 | 1.80  |
    | 0.60  | 3.50  | 1.40 | 1.80  |
    | 0.10  | 3.00  | 1.50 | 2.00  |
    | 0.20  | 3.10  | 1.50 | 2.00  |
    | 0.30  | 3.20  | 1.50 | 2.00  |
    | 0.40  | 3.30  | 1.50 | 2.00  |
    | 0.50  | 3.40  | 1.50 | 2.00  |
    | 0.60  | 3.50  | 1.50 | 2.00  |



