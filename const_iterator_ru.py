from typing import List, Tuple, Dict, Union, Generator, Any

_DICT_OF_CONSTANTS = Dict[str, Tuple[Union[int, float], Union[int, float, None], Union[int, float, None]]]


def _gen_value(start, stop=0, step=0):
    """Сгенерировать последлвательность значений константы.

    Работает как оригинальный range, только с float.
    Проблема есть из-за точности float, иногда последнее значение
    не считается из-за того, что там хвост .000000005 и т.п.

    :argument start начальное значение константы
    :argument stop максимальное значение константы
    :argument step шаг
    """
    value = float(start)
    stop = float(stop)
    step = float(step)

    yield start

    while True:
        value += step
        if value > stop or step == 0:
            break
        yield value


def _gen_constants_values(constants_info):
    """Сгенерировать набор констант.

    За одну итерацию одновременно изменяет значения всех констант.

    :argument constants_info словарь, где
        key - название константы,
        value - кортеж информации для генерации констант.

    :return Словарь констант, где key-название константы, value-значение константы.
    """
    constants_generators = {name: _gen_value(*info) for name, info in constants_info.items()}
    result = {}
    while True:
        try:
            for name, generator in constants_generators.items():
                result[name] = next(generator)
        except StopIteration:
            break
        yield result


def _get_constant_group(constants_groups, const):
    """Вернуть названия всех констант из группы константы."""
    if constants_groups:
        for group in constants_groups:
            if const in group:
                const_name_index = group.index(const)
                return group[:const_name_index] + group[const_name_index + 1:]


def _get_constants_generator(dict_of_constants_info, constants_groups=None, **kwargs)\
        -> Generator[Dict[str, float], Any, None]:
    """Сгенерировать словарь констант.

    Работает рекурсивно, в начале спускается по рекурсии
    к последней константе, проходит по ее генератору полностью,
    затем делает шаг назад, изменяя предпоследлюю константу и т.д.
    Таким образом возвращает все возможные наборы констант.

    Если передается constants_groups, то группы констант
    изменяются одновреммено за 1 итерацию.
        :return: За одну итерацию возвращает словарь констант
            key - название константы,
            value - значение константы.
    """
    cur_const_info = dict_of_constants_info.popitem()
    constants_to_get_on_this_step = {cur_const_info[0]: cur_const_info[1]}

    constants_from_the_same_group = _get_constant_group(constants_groups, cur_const_info[0])
    if constants_from_the_same_group:
        for const_name in constants_from_the_same_group:
            const_info = dict_of_constants_info.pop(const_name)
            constants_to_get_on_this_step[const_name] = const_info

    for constants_values in _gen_constants_values(constants_to_get_on_this_step):
        kwargs.update(constants_values)

        if dict_of_constants_info:
            for full_set_of_required_constants in _get_constants_generator(
                    dict_of_constants_info.copy(), constants_groups, **kwargs):
                yield full_set_of_required_constants
        else:
            yield kwargs


class ConstantsIterator:
    """Итератор, который генерирует словари констант.

    Работает рекурсивно, в начале спускается по рекурсии
    к последней константе, проходит по ее генератору полностью,
    затем делает шаг назад, изменяя предпоследлюю константу и т.д.
    Таким образом возвращает все возможные наборы констант.
    Если передается constants_groups, то группы констант
    изменяются одновреммено за 1 итерацию.
    """

    def __init__(
            self,
            dict_of_constants_info: _DICT_OF_CONSTANTS,
            groups_of_constants: List[Tuple[str, ...]] = None):
        """
        :param dict_of_constants_info - Словарь констант.
            Ключом словаря должно являться название константы
            Значением должен являться кортеж состоящий из 3 чисел int или float:
            1 число - Начальное значение константы;
            2 число - Конечное значение константы;
            3 число - Шаг изменения константы.
            Пример:
                dict_of_constants = {
                    'alpha': (0.1, 0.5, 0.1),
                    'beta': (1, 1.6, 0.1),
                    'gamma': (3, 3.5, 0.1),
                }
            Если необходимо, чтобы константа не изменялась, тогда
            просто введите кортеж из 1 элемента(не забывайте запятую после значения)
            Пример:
                dict_of_constants = {
                    'alpha': (0.1,),
                    'beta': (1, 1.6, 0.1),
                    'gamma': (3, 3.5, 0.1),
                }
        :param groups_of_constants - список групп констант.
            Является необязательным аргументом.
            Группы должны быть переданы кортежем и
            состоять из названий констант переданных в словаре.
            Необходимо использовать если нужно, чтобы некоторые константы
            изменялись одновременно за одну итерацию.
            Пример: groups_of_constants = [('alpha','gamma')]
        """
        self.groups_of_constants = groups_of_constants
        self.dict_of_constants = dict_of_constants_info

    def __iter__(self):
        """
        Возвращает генератор при вызове с помощью
        for constants in ConstantsIterator(dict_of_constants_info):
            ...
        constants - словарь констант.
        Или с помощью iter(ConstantsIterator(dict_of_constants_info)).
        :return: генератор словарей констант
        """
        return _get_constants_generator(self.dict_of_constants, self.groups_of_constants)


if __name__ == '__main__':
    dict_of_constants = {
        'alpha': (0.1, 0.6, 0.1),
        'beta': (1, 1.61, 0.1),
        'gamma': (3, 3.51, 0.1),
        'delta': (1, 2, 0.2),
    }
    groups = [('alpha', 'gamma'), ('beta', 'delta')]

    hat = '| alpha | gamma | beta | delta |'
    print(hat)
    for constants in ConstantsIterator(dict_of_constants, groups):
        row = '|{alpha:^7.2f}|{gamma:^7.2f}|{beta:^6.2f}|{delta:^7.2f}|'.format(**constants)
        print(row)
